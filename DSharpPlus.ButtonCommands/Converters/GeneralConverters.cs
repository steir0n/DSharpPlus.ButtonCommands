#pragma warning disable 1998
using System.Threading.Tasks;
using DSharpPlus.Entities;

namespace DSharpPlus.ButtonCommands.Converters
{
	public class StringConverter : IButtonArgumentConverter<string>
	{
		public async Task<Optional<string>> ConvertAsync(string value, ButtonContext ctx) => value;

		public string ConvertToString(string value) => value; // i deserve to get paid for this line of code
	}

	public class BoolConverter : IButtonArgumentConverter<bool>
	{
		public async Task<Optional<bool>> ConvertAsync(string value, ButtonContext ctx)
		{
			switch (value)
			{
				case "true":
				case "t":
				case "yes":
				case "y":
				case "1":
					return Optional.FromValue(true);
				case "false":
				case "f":
				case "no":
				case "n":
				case "0":
					return Optional.FromValue(false);
				default:
					return Optional.FromNoValue<bool>();
			}
		}

		public string ConvertToString(bool value) => value.ToString();
	}

	public class IntConverter : IButtonArgumentConverter<int>
	{
		public async Task<Optional<int>> ConvertAsync(string value, ButtonContext ctx)
		{
			return int.TryParse(value, out int res) ? Optional.FromValue(res) : Optional.FromNoValue<int>();
		}

		public string ConvertToString(int value) => value.ToString();
	}

	public class UintConverter : IButtonArgumentConverter<uint>
	{
		public async Task<Optional<uint>> ConvertAsync(string value, ButtonContext ctx)
		{
			return uint.TryParse(value, out uint res) ? Optional.FromValue(res) : Optional.FromNoValue<uint>();
		}

		public string ConvertToString(uint value) => value.ToString();
	}

	public class LongConverter : IButtonArgumentConverter<long>
	{
		public async Task<Optional<long>> ConvertAsync(string value, ButtonContext ctx)
		{
			return long.TryParse(value, out long res) ? Optional.FromValue(res) : Optional.FromNoValue<long>();
		}

		public string ConvertToString(long value) => value.ToString();
	}

	public class UlongConverter : IButtonArgumentConverter<ulong>
	{
		public async Task<Optional<ulong>> ConvertAsync(string value, ButtonContext ctx)
		{
			return ulong.TryParse(value, out ulong res) ? Optional.FromValue(res) : Optional.FromNoValue<ulong>();
		}

		public string ConvertToString(ulong value) => value.ToString();
	}

	public class FloatConverter : IButtonArgumentConverter<float>
	{
		public async Task<Optional<float>> ConvertAsync(string value, ButtonContext ctx)
		{
			return float.TryParse(value, out float res) ? Optional.FromValue(res) : Optional.FromNoValue<float>();
		}

		public string ConvertToString(float value) => value.ToString();
	}

	public class DoubleConverter : IButtonArgumentConverter<double>
	{
		public async Task<Optional<double>> ConvertAsync(string value, ButtonContext ctx)
		{
			return double.TryParse(value, out double res) ? Optional.FromValue(res) : Optional.FromNoValue<double>();
		}

		public string ConvertToString(double value) => value.ToString();
	}
}