using System;
using Emzi0767.Utilities;

namespace DSharpPlus.ButtonCommands.EventArgs
{
	public class ButtonCommandExecutionEventArgs : AsyncEventArgs
	{
		public string ButtonId { get; set; }
		public string CommandName { get; set; }
		public ButtonContext Context { get; set; }
	}
}