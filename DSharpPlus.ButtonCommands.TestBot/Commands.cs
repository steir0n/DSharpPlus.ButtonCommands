using System.Threading.Tasks;
using DSharpPlus.ButtonCommands.Extensions;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;

namespace DSharpPlus.ButtonCommands.TestBot
{
	public class Commands : BaseCommandModule
	{
		[Command("buttons")]
		public async Task Buttons(CommandContext context)
		{
			ButtonCommandsExtension bc = context.Client.GetButtonCommands();
			DiscordMessageBuilder messageBuilder = new DiscordMessageBuilder()
				.WithContent("Buttons!!")
				.AddComponents(
					new DiscordButtonComponent(ButtonStyle.Primary, bc.BuildButtonId("example"), "Example"),
					new DiscordButtonComponent(ButtonStyle.Secondary, bc.BuildButtonId("user", context.Client.CurrentUser), "Button with user argument"),
					new DiscordButtonComponent(ButtonStyle.Secondary, bc.BuildButtonId("user.string", context.Channel, "Test"), "Button with channel and string argument"),
					new DiscordButtonComponent(ButtonStyle.Secondary, bc.BuildButtonId("di"), "Dependency Injection!"),
					new DiscordButtonComponent(ButtonStyle.Danger, bc.BuildButtonId("message", context.Message), "Button with custom converter")
				);

			await context.RespondAsync(messageBuilder);
		}
		
	}
}